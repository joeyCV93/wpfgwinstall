﻿/// <summary>
/// The 'Builder' interface
/// </summary>
public interface ICenterBuilder
{
    void SetURLs();
  

    Center GetCenter();
}

/// <summary>
/// The 'ConcreteBuilder1' class
/// </summary>
public class ContactManagerBuilder : ICenterBuilder
{
    Center centerObject = new Center();
    public void SetURLs()
    {
        centerObject.InspectionsProfileDirectoryToMoveTo = "";
        centerObject.InspectionsProfileDirectoryToMoveTo = "";
        centerObject.GWDirectorypath = "";
        centerObject.CentersLocation = "";
        centerObject.DBName = "";
        centerObject.DBNamesList = "";
        centerObject.SVNConfigPath = "";
        centerObject.CMSVNDevBranchURLs = "";
        centerObject.BCSVNDevBranchURLs = "";
        centerObject.CCSVNDevBranchURLs = "";
        centerObject.PCSVNDevBranchURLs = "";
        centerObject.CMSVNStashConfigPaths = "";
        centerObject.BCSVNStashConfigPaths = "";
        centerObject.CCSVNStashConfigPaths = "";
        centerObject.PCSVNStashConfigPaths = "";
        centerObject.CenterBranchAddedName = "";
        centerObject.CenterName = "";
        centerObject.ConfigurationFolderPath = "";
        centerObject.DBBackupDestinationPath = "";
        centerObject.ArchivePath = "";
        centerObject.PluginsPath = "";
        centerObject.Centres = "";
        centerObject.CenterIndex = "";
        centerObject.BooleanArray = "";
        centerObject.SelectedCMBranchRevisions = "";
        centerObject.SelectedBCBranchRevisions = "";
        centerObject.SelectedCCBranchRevisions = "";
        centerObject.SelectedPCBranchRevisions = "";
        centerObject.RevToPull = "";
        centerObject.CentersBranchName = "";
        centerObject.CenterNameNoNumbers = "";
        centerObject.AutoLaunchBool = "";
        centerObject.RegenDocsBool = "";
        centerObject.ReplacementConnection = "";
        centerObject.DBNameTypeBool = "";
        centerObject.BranchArray = "";
        centerObject.DBBackupURL = "";
        centerObject.DBNameSelectionBoxArray = "";
        centerObject.DBNameTextBoxArray = "";
        centerObject.DBNameUsed = "";
        centerObject.CMBranchURLList = "";
        centerObject.BCBranchURLList = "";
        centerObject.CCBranchURLList = "";
        centerObject.PCBranchURLList = "";
    }


    public Center GetCenter()
    {
        return centerObject;
    }
}
public class BillingCenterBuilder { }
public class ClaimCenterBuilder { }
public class PolicyCenterBuilder { }

/// <summary>
/// The 'Product' class
/// </summary>
public class Center
{
    string InspectionsProfileDirectoryToMoveTo { get; set; }
    string GWDirectorypath { get; set; }
    string CentersLocation { get; set; }
    string DBName { get; set; }
    public List<string> DBNamesList { get; set; }
    public List<string> SVNConfigPath { get; set; }
    public List<string> CMSVNDevBranchURLs { get; set; }
    public List<string> BCSVNDevBranchURLs { get; set; }
    public List<string> CCSVNDevBranchURLs { get; set; }
    public List<string> PCSVNDevBranchURLs { get; set; }
    public List<string> CMSVNStashConfigPaths { get; set; }
    public List<string> BCSVNStashConfigPaths { get; set; }
    public List<string> CCSVNStashConfigPaths { get; set; }
    public List<string> PCSVNStashConfigPaths { get; set; }
    string CenterBranchAddedName { get; set; }
    string CenterName { get; set; }
    string ConfigurationFolderPath { get; set; }
    string DBBackupDestinationPath { get; set; }
    string ArchivePath { get; set; }
    string PluginsPath { get; set; }
    public List<string> Centres { get; set; }
    public List<string> CenterIndex { get; set; }
    public List<string> BooleanArray { get; set; }
    public List<string> SelectedCMBranchRevisions { get; set; }
    public List<string> SelectedBCBranchRevisions { get; set; }
    public List<string> SelectedCCBranchRevisions { get; set; }
    public List<string> SelectedPCBranchRevisions { get; set; }
    public List<string> RevToPull { get; set; }
    string CentersBranchName { get; set; }
    string CenterNameNoNumbers { get; set; }
    string AutoLaunchBool { get; set; }
    string RegenDocsBool { get; set; }
    string ReplacementConnectionString { get; set; }
    string DBNameTypeBool { get; set; }
    public List<string> BranchArray { get; set; }
    public List<string> DBBackupURL { get; set; }
    public List<string> DBNameSelectionBoxArray { get; set; }
    public List<string> DBNameTextBoxArray { get; set; }
    public List<string> DBNameUsed { get; set; }
    public List<string> CMBranchURLList { get; set; }
    public List<string> BCBranchURLList { get; set; }
    public List<string> CCBranchURLList { get; set; }
    public List<string> PCBranchURLList { get; set; }

    public Center()
    {
        string InspectionsProfileDirectoryToMoveTo = @"C:\Users\chilstj\Desktop\GWInstallTest\ContactManager807\.idea\settings\config\inspection";
        string GWDirectorypath = "C:/GuidewireTEST";
        string CentersLocation = "C:/Users/chilstj/Desktop/GW_Installer_Project"; //"H:\IT\Delivery\GuidewireUpgrade20190405";##Check 
        DBNamesList = new List<string>();
        SVNConfigPath = new List<string>();
        CMSVNDevBranchURLs = new List<string>();
        BCSVNDevBranchURLs = new List<string>();
        CCSVNDevBranchURLs = new List<string>();
        PCSVNDevBranchURLs = new List<string>();
        CMSVNStashConfigPaths = new List<string>();
        BCSVNStashConfigPaths = new List<string>();
        CCSVNStashConfigPaths = new List<string>();
        PCSVNStashConfigPaths = new List<string>();
        Centres = new List<string>{ "ContactManager807", "BillingCenter807", "ClaimCenter807", "PolicyCenter808", };
        CenterIndex = new List<string>();
        BooleanArray = new List<string>();
        SelectedCMBranchRevisions = new List<string>();
        SelectedBCBranchRevisions = new List<string>();
        SelectedCCBranchRevisions = new List<string>();
        SelectedPCBranchRevisions = new List<string>();
        RevToPull = new List<string>();
        BranchArray = new List<string>();
        DBBackupURL = new List<string> { "http://bx-cinappd02.network.uk.ad:8081/artifactory/Database/com/hastingsdirect/MasterBackup_PR1905_28May2019/RM2-20190528041012-MasterBackup_PR1905_28May2019/RMGW02contactManagerDB_FULL.bak", "http://bx-cinappd02.network.uk.ad:8081/artifactory/Database/com/hastingsdirect/MasterBackup_PR1905_28May2019/RM2-20190528041012-MasterBackup_PR1905_28May2019/RMGW02billingCenterDB_FULL.bak", "http://bx-cinappd02.network.uk.ad:8081/artifactory/Database/com/hastingsdirect/MasterBackup_PR1905_28May2019/RM2-20190528041012-MasterBackup_PR1905_28May2019/RMGW02claimCenterDB_FULL.bak", "http://bx-cinappd02.network.uk.ad:8081/artifactory/Database/com/hastingsdirect/MasterBackup_PR1905_28May2019/RM2-20190528041012-MasterBackup_PR1905_28May2019/RMGW02policyCenterDB_FULL.bak" };
            DBNameSelectionBoxArray = new List<string>();
            DBNameTextBoxArray = new List<string>();
            DBNameUsed = new List<string>();
            CMBranchURLList = new List<string>();
            BCBranchURLList = new List<string>();
            CCBranchURLList = new List<string>();
            PCBranchURLList = new List<string>();

        }

    }

    /// <summary>
    /// The 'Director' class
    /// </summary>
    public class CenterCreator
    {
        private readonly ICenterBuilder objBuilder;

        public CenterCreator(ICenterBuilder builder)
        {
            objBuilder = builder;
        }

        public void CreateCenter()
        {
            objBuilder.SetURLs();

        }

        public Center GetCenter()
        {
            return objBuilder.GetCenter();
        }
    }


/// <summary>
/// Builder Design Pattern Demo
/// </summary>
//class Program
//{
//    static void Main(string[] args)
//    {
//        var CenterCreator = new CenterCreator(new HeroBuilder());
//        CenterCreator.CreateCenter();
//        var Center = CenterCreator.GetCenter();
//        Center.ShowInfo();

//        Console.WriteLine("---------------------------------------------");

//        CenterCreator = new CenterCreator(new HondaBuilder());
//        CenterCreator.CreateCenter();
//        Center = CenterCreator.GetCenter();
//        Center.ShowInfo();

//        Console.ReadKey();
//    }
//}